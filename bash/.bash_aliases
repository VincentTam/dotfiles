# Automatically xclip to keyboard paste
alias xclip='xclip -selection c'
alias lm="ls -al | more"
alias md="mkdir"
alias rd="rmdir"
