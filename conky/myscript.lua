-- Find the right `i` for `${hwmon [i] temp n}`
function conky_cputemp(n)
    return conky_parse("${hwmon "..findHwmonNum().." temp "..n.."}")
end

-- Find local IP address according to active connections
function conky_myaddr()
  return conky_parse('${addr ' .. findInterface() .. '}')
end

-- Find up/download speed
function conky_myspeed(upordown)
  print(findInterface())
  return conky_parse('${' .. upordown .. 'speed ' .. findInterface() .. '}')
end

-- Generate up/download speed graph
function conky_myspeedgraph(upordown, h, w)
  return conky_parse('${'..upordown..'speedgraph '..findInterface()..' '..h..','..w..'}')
end

-- Test if /sys/class/hwmon/hwmonI/temp3_crit exists for I=0,...,10
-- Increase `10` to cover the number of directories /sys/class/hwmon/hwmonN
function findHwmonNum()
    for i = 0,10,1 do
        if file_exists("/sys/class/hwmon/hwmon"..i.."/temp3_crit") then
            return i
        end
    end
end

-- https://stackoverflow.com/a/4991602/3184351
function file_exists(name)
   local f=io.open(name,"r")
   if f~=nil then io.close(f) return true else return false end
end

-- Find the string representing the connection's interface (e.g.  eth0)
function findInterface()
  local handle = io.popen('ip a | grep "state UP" | cut -d: -f2 | tr -d " "')
  local result = handle:read('*a'):gsub('\n$','')
  handle:close()
  return result
end
