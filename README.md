My Dotfiles
===

This repo holds my config files for

- Bash
- Git
- SSH
- GNU PG
- Sublime Text 3
- Vi(m-tiny)

GNU Stow is the easiest way that I know for managing dotfiles.  To
avoid accidentally publishing GPG keys, I used [gibo][1] to automatically
generate a global `.gitignore` file from the
[templates prepared by GitHub][2].

[1]: https://github.com/simonwhitaker/gibo
[2]: https://github.com/github/gitignore
